$(document).ready(function() {
    // $('#myTable').DataTable();
    loadtable();

    $(document).on('click', ".del", function() {
        var del_id = $(this).attr("id");
        $('#delmodal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $("#del_id").val(del_id);
    })
    $(".alert").find(".close").on("click", function(e) {
        // Find all elements with the "alert" class, get all descendant elements with the class "close", and bind a "click" event handler
        e.stopPropagation(); // Don't allow the click to bubble up the DOM
        e.preventDefault(); // Don't let any default functionality occur (in case it's a link)
        $(this).closest(".alert").slideUp(400); // Hide this specific Alert
    });

});

function del_admin() {
    var del_id = $("#del_id").val();
    $.ajax({
        data: { del_id: del_id },
        method: "post",
        url: "../controller/del-admin.php",
        success: function(result) {
            var data = JSON.parse(result);
            $('#delmodal').modal('hide');
            if (data.error) {
                $("msgd").html(data.data);
                $("#formAlertd").slideDown(400);
            } else {
                $("#msgs").html(data.data);
                $("#formAlerts").slideDown(400);
            }
            loadtable();
        }
    });

}

// function del_admin() {
//     var del_id = $("#del_id").val();
//     $.ajax({
//         data: { del_id: del_id },
//         method: "post",
//         url: "../controller/del-admin.php",
//         success: function(result) {
//             var data = JSON.parse(result);
//             $('#delmodal').modal('hide');
//             if (data.error) {
//                 $("msgd").html(data.data);
//                 $("#formAlertd").slideDown(400);
//             } else {
//                 $("#msgs").html(data.data);
//                 $("#formAlerts").slideDown(400);
//             }
//             loadtable();
//         }
//     });

// }


function loadtable() {
    var output = "";
    $.ajax({
        url: "../controller/get-admins.php",
        success: function(result) {
            var data = JSON.parse(result);

            if (data.error) {
                alert(data.data);
            } else {

                jQuery.each(data.data, function(i, val) {
                    output += "<tr>";
                    output += "<td>" + val.first_name + "</td>";
                    output += "<td>" + val.last_name + "</td>";
                    output += "<td>" + val.email + "</td>";
                    if (data.login_by == val.admin_id) {
                        output += "<td><a href='admin-view.php?id=" + val.admin_id + "' class='btn btn-primary '><i class='fa fa-eye'> </i></a></td>";
                    } else {
                        output += "<td><a href='admin-view.php?id=" + val.admin_id + "' class='btn btn-primary '><i class='fa fa-eye'> </i></a>&nbsp;&nbsp;<a href='admin-edit-view.php?id=" + val.admin_id + "' class='btn btn-success '><i class='fa fa-pencil'> </i></a>&nbsp;&nbsp;<a href ='#' id='" + val.admin_id + "' class='btn btn-danger del'><i class='fa fa-trash'> </i></a></td>";
                    }

                    output += "</tr>";
                });
            }
            $("#data").html(output);
            $('#myTable').DataTable();
        }
    });
}