var phone_err = false
var email_err = false
$(document).ready(function() {
    // $('#myTable').DataTable();

    loadadmin();
    // $('form[name="register"]').on("submit", function(e) {
    //     // Find all <form>s with the name "register", and bind a "submit" event handler

    //     // Find the <input /> element with the name "username"
    //     var first_name = $(this).find('input[name="first_name"]');
    //     var last_name = $(this).find('input[name="last_name"]');
    //     var email = $(this).find('input[name="email"]');
    //     var phone = $(this).find('input[name="phone"]');
    //     var password = $(this).find('input[name="password"]');
    //     var role_id = $(this).find('input[name="role_id"]');
    //     var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    //     if ($.trim(first_name.val()) === "") {

    //         e.preventDefault();
    //         $("#error-msg").html("Please Enter First Name");
    //         $("#formAlert").slideDown(400);
    //     } else if ($.trim(first_name.val()).length > 16) {

    //         e.preventDefault();
    //         $("#error-msg").html("First Name Maximum Charachter limit is 16");
    //         $("#formAlert").slideDown(400);
    //     } else if ($.trim(first_name.val()).length < 4) {

    //         e.preventDefault();
    //         $("#error-msg").html("Please Enter atleast 4 character in First Name");
    //         $("#formAlert").slideDown(400);
    //     } else if ($.trim(last_name.val()) === "") {

    //         e.preventDefault();
    //         $("#error-msg").html("Please Enter Last Name");
    //         $("#formAlert").slideDown(400);
    //     } else if ($.trim(last_name.val()).length > 16) {

    //         e.preventDefault();
    //         $("#error-msg").html("Last Name Maximum Charachter limit is 16");
    //         $("#formAlert").slideDown(400);
    //     } else if ($.trim(last_name.val()).length < 4) {

    //         e.preventDefault();
    //         $("#error-msg").html("Please Enter atleast 4 character in Last Name");
    //         $("#formAlert").slideDown(400);
    //     } else if ($.trim(email.val()) === "") {

    //         e.preventDefault();
    //         $("#error-msg").html("Please Enter Email Address");
    //         $("#formAlert").slideDown(400);
    //     } else if (!$.trim(email.val()).match(mailformat)) {

    //         // alert();
    //         e.preventDefault();
    //         $("#error-msg").html("Please Enter Valid Email Address");
    //         $("#formAlert").slideDown(400);
    //     } else if ($.trim(phone.val()) === "") {

    //         e.preventDefault();
    //         $("#error-msg").html("Please Enter Phone");
    //         $("#formAlert").slideDown(400);
    //     } else if (isNaN($.trim(phone.val()))) {

    //         e.preventDefault();
    //         $("#error-msg").html("Please Enter only numric in phone ");
    //         $("#formAlert").slideDown(400);
    //     } else if ($.trim(phone.val()).length != 10) {

    //         e.preventDefault();
    //         $("#error-msg").html("Please Enter only 10 digits in phone ");
    //         $("#formAlert").slideDown(400);
    //     } else if ($.trim(password.val()) === "") {

    //         e.preventDefault();
    //         $("#error-msg").html("Please Enter Password");
    //         $("#formAlert").slideDown(400);
    //     } else if ($.trim(role_id.val()) === "0") {

    //         e.preventDefault();
    //         $("#error-msg").html("Please Enter Role");
    //         $("#formAlert").slideDown(400);
    //     } else if (phone_err) {
    //         e.preventDefault();
    //         $("#error-msg").html("Phone number already exists");
    //         $("#formAlert").slideDown(400);
    //     } else if (email_err) {
    //         e.preventDefault();
    //         $("#error-msg").html("Email already exists");
    //         $("#formAlert").slideDown(400);
    //     } else {

    //         $("#formAlert").slideUp(400, function() {

    //         });
    //     }
    // });

    // $(".alert").find(".close").on("click", function(e) {
    //     // Find all elements with the "alert" class, get all descendant elements with the class "close", and bind a "click" event handler
    //     e.stopPropagation(); // Don't allow the click to bubble up the DOM
    //     e.preventDefault(); // Don't let any default functionality occur (in case it's a link)
    //     $(this).closest(".alert").slideUp(400); // Hide this specific Alert
    // });
});

function check_phone(phone) {
    var ans;
    $.ajax({
        url: "../controller/check-phone.php",
        data: { phone: phone },
        method: "post",
        success: function(result) {
            // alert()
            if (result == "true") {
                phone_err = true;
                $("#error-msg").html("Phone number already exists");
                $("#formAlert").slideDown(400);
            } else {
                phone_err = false;
                $("#formAlert").slideUp(400, function() {

                });
            }


        },
        error: function() {
            alert();
        }
    });
    // return ans;
}

function check_email(email) {
    var ans;
    $.ajax({
        url: "../controller/check-email.php",
        data: { email: email },
        method: "post",
        success: function(result) {
            // alert()
            if (result == "true") {
                email_err = true;
                $("#error-msg").html("Email already exists");
                $("#formAlert").slideDown(400);
            } else {
                email_err = false;
                $("#formAlert").slideUp(400, function() {

                });
            }


        },
        error: function() {
            alert();
        }
    });
    // return ans;
}

function loadadmin() {
    var view_id = $("#view_id").val();
    // alert(view_id);
    var output = "<option value='0'>Select Role</option>";
    $.ajax({
        url: "../controller/view-admin.php?id=" + view_id,
        success: function(result) {
            var data = JSON.parse(result);

            if (data.error) {
                alert(data.data);
            } else {
                // alert(data.data[1]);
                $("#first_name").html(data.data[2]);
                $("#last_name").html(data.data[3]);
                $("#email").html(data.data[4]);
                $("#phone").html(data.data[5]);
                $("#role").html(data.data[9]);
                // jQuery.each(data.data, function(i, val) {
                //     if (val.role != "") {
                //         output += "<option value='" + val.role_id + "'>" + val.role + "</option>";
                //     }
                // });
            }
            $("#role_id").html(output);
            // $('#myTable').DataTable();
        }
    });
}