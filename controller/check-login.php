<?php
include "../global.php";
session_start();
if (isset($_POST["email"]) && isset($_POST["password"])) {
    $email = $_POST["email"];
    $password = $_POST["password"];
    $md5_pass = md5($password);

    $db = connection();


    // check login 
    if (!$db) {

        $message = "Unable to open database";
        echo "<script type='text/javascript'>confirm('$message');window.location.href = '" . base_url() . "';</script>";
    } else {
        // Opened database successfully
        $sql = <<<EOF
      SELECT * FROM admin_master where email ='$email' and password = '$md5_pass';
EOF;

        $ret = pg_query($db, $sql);
        if (!$ret) {
            $message = pg_last_error($db);
            echo "<script type='text/javascript'>confirm('$message');window.location.href = '" . base_url() . "';</script>";
        } else {
            $num_rows = pg_num_rows($ret);
            // 
            if ($num_rows > 0) {
                while ($row = pg_fetch_row($ret)) {
                    $_SESSION["admin_id"] = $row[0];
                    $_SESSION["first_name"] = $row[1];
                    $_SESSION["last_name"] = $row[2];
                }

                header("Location: " . base_url() . "controller/dashboard.php");
            } else {
                $message = "Invalid Email or Password";
                echo "<script type='text/javascript'>confirm('$message');window.location.href = '" . base_url() . "';</script>";
            }
        }

        // conection close 
        pg_close($db);
    }
} else {
    // echo base_url();
    header("Location: " . base_url());
}
