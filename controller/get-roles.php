<?php
include "../global.php";
session_start();
$db = connection();
//get admins
if (!$db) {

    $message = "Unable to open database";
    $data = array("error"=>true, "data"=>$message);
    echo json_encode($data);
} else {
    // Opened database successfully
    $sql = <<<EOF
  SELECT * FROM role_master ;
EOF;

    $ret = pg_query($db, $sql);
    if (!$ret) {
        $message = pg_last_error($db);
        $data = array("error"=>true, "data"=>$message);
        echo json_encode($data);
    } else {
        $result = pg_fetch_all($ret);
        $data = array("error"=>false, "data"=>$result , "login_by"=>$_SESSION["admin_id"]);
        echo json_encode($data);
    }
}
