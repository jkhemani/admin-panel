<?php
include "../global.php";
session_start();
$db = connection();
//get admins
if ($_SESSION["admin_id"]) {
    // echo "Hello ".$_SESSION["first_name"];
    if (isset($_GET['id'])) {
        if (!$db) {

            $message = "Unable to open database";
            $data = array("error" => true, "data" => $message);
            echo json_encode($data);
        } else {
            // Opened database successfully
            $id = $_GET['id'];
            $sql = <<<EOF
      SELECT * FROM admin_master join admin_role using(admin_id) join role_master using(role_id) where admin_id=$id;
    EOF;

            $ret = pg_query($db, $sql);
            if (!$ret) {
                $message = pg_last_error($db);
                $data = array("error" => true, "data" => $message);
                echo json_encode($data);
            } else {
                $result = pg_fetch_row($ret);
                $data = array("error" => false, "data" => $result, "login_by" => $_SESSION["admin_id"]);
                echo json_encode($data);
            }
        }
    }
    include "../view/admin-view.html";
} else {
    header("Location: " . base_url());
}
