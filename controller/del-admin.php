<?php
include "../global.php";
session_start();

if ($_POST["del_id"]) {
    $del_id = $_POST["del_id"];
    $db = connection();
    //get admins
    if (!$db) {

        $message = "Unable to open database";
        $data = array("error" => true, "data" => $message);
        echo json_encode($data);
    } else {
        // Opened database successfully
        $sql = <<<EOF
  delete  FROM admin_master where admin_id = $del_id;
EOF;

        $ret = pg_query($db, $sql);
        if (!$ret) {
            $message = pg_last_error($db);
            $data = array("error" => true, "data" => $message);
            echo json_encode($data);
        } else {
            $sql = <<<EOF
  delete  FROM admin_role where admin_id = $del_id;
EOF;

            $ret = pg_query($db, $sql);
            if (!$ret) {
                $message = pg_last_error($db);
                $data = array("error" => true, "data" => $message);
                echo json_encode($data);
            } else {
                $result = pg_fetch_all($ret);
                $data = array("error" => false, "data" => $result, "login_by" => $_SESSION["admin_id"]);
                echo json_encode($data);
            }
        }
    }
}
